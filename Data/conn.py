import pyodbc
import conn_vars as vars

def get_connection(server):
    conn = pyodbc.connect("DRIVER={NetezzaSQL};"
                          "SERVER=" + vars.servers[server] + ";"
                          "PORT=" + vars.PORT + ";"
                          "DATABASE=" + vars.DATABASE + ";"
                          "UID=" + vars.UID + ";"
                          "PWD=" + vars.PWD + ";")
    return conn

