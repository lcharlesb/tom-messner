import pandas as pd
import query_builder as qb
import conn




def get_df_from_sql(server, sql):
    connection = conn.get_connection(server)

    query = pd.read_sql_query(sql, connection)
    df = pd.DataFrame(query)

    connection.close()

    return df

def get_df_by_zip(server, schema):
    connection = conn.get_connection(server)

    # Use query_builder to get sql
    sql = qb.zip_sql(schema)

    # Execute sql and put in dataframe
    query = pd.read_sql_query(sql, connection)
    df = pd.DataFrame(query)

    # Close connection and return dataframe
    connection.close()
    return df

def get_profit_by_market_type_by_month(server, schema):
    connection = conn.get_connection(server)

    # Get markets that schema has entries for
    sql_markets = 'select distinct COTID from ' + schema + '.idiget1.mqt30'
    markets = pd.read_sql_query(sql_markets, connection)['COTID'].values.tolist()

    # Use query_builder to get sql
    sql = qb.profit_by_market_type_by_month_sql(schema, markets)

    # Execute sql and put in dataframe
    query = pd.read_sql_query(sql, connection)
    df = pd.DataFrame(query)

    # Close connection and return dataframe
    connection.close()
    return df

def get_cumulative_profit_by_market_type_by_month(server, schema):
    connection = conn.get_connection(server)

    # Get years that schema has entries for
    sql_years = 'select distinct substring(DATE, 1, 4) as year from ' + schema + '.idiget1.mqt30 order by year desc'
    years = pd.read_sql_query(sql_years, connection)['YEAR'].values.tolist()

    # Use query_builder to get sql
    sql = qb.cumulative_profit_by_market_type_by_month_sql(schema, years)

    # Execute sql and put in dataframe
    query = pd.read_sql_query(sql, connection)
    df = pd.DataFrame(query)

    # Close connection and return dataframe
    connection.close()
    return df

def get_profit_by_month(server, schema):
    connection = conn.get_connection(server)

    # Use query_builder to get sql
    sql = qb.profit_by_month(schema)

    # Execute sql and put in dataframe
    query = pd.read_sql_query(sql, connection)
    df = pd.DataFrame(query)

    # Close connection and return dataframe
    connection.close()
    return df

def get_profit_ces_by_month(server, schema):
    connection = conn.get_connection(server)

    # Use query_builder to get sql
    sql = qb.profit_ces_by_month(schema)

    # Execute sql and put in dataframe
    query = pd.read_sql_query(sql, connection)
    df = pd.DataFrame(query)

    # Close connection and return dataframe
    connection.close()
    return df

def get_profit_ces_by_day(server, schema):
    connection = conn.get_connection(server)

    # Use query_builder to get sql
    sql = qb.profit_ces_by_day(schema)

    # Execute sql and put in dataframe
    query = pd.read_sql_query(sql, connection)
    df = pd.DataFrame(query)

    # Close connection and return dataframe
    connection.close()
    return df

def get_profit_ces_qty_by_day(server, schema):
    connection = conn.get_connection(server)

    # Use query_builder to get sql
    sql = qb.profit_ces_qty_by_day(schema)

    # Execute sql and put in dataframe
    query = pd.read_sql_query(sql, connection)
    df = pd.DataFrame(query)

    # Close connection and return dataframe
    connection.close()
    return df

def get_profit_ces_qty_depletion_by_day(server, schema):
    connection = conn.get_connection(server)

    # Use query_builder to get sql
    sql = qb.profit_ces_qty_depletion_by_day(schema)

    # Execute sql and put in dataframe
    query = pd.read_sql_query(sql, connection)
    df = pd.DataFrame(query)

    # Close connection and return dataframe
    connection.close()
    return df

def get_profit_ces_qty_by_week(server, schema):
    connection = conn.get_connection(server)

    # Use query_builder to get sql
    sql = qb.profit_ces_qty_by_week(schema)

    # Execute sql and put in dataframe
    query = pd.read_sql_query(sql, connection)
    df = pd.DataFrame(query)

    # Close connection and return dataframe
    connection.close()
    return df

def get_brand_family_by_day(server, schema, brand_family):
    connection = conn.get_connection(server)

    # Get brands that brand family has entries for
    sql_brands = 'select distinct BRDID from ' + schema + '.idiget1.dimitem where CAT2ID = \'' + brand_family + '\' order by BRDID asc;'
    brands = pd.read_sql_query(sql_brands, connection)['BRDID'].values.tolist()

    # Use query_builder to get sql
    sql = qb.brand_family_by_day(schema, brand_family, brands)

    # Execute sql and put in dataframe
    query = pd.read_sql_query(sql, connection)
    df = pd.DataFrame(query)

    # Close connection and return dataframe
    connection.close()
    return df

def get_profit_by_day(server, schema):
    connection = conn.get_connection(server)

    # Use query_builder to get sql
    sql = qb.profit_by_day(schema)

    # Execute sql and put in dataframe
    query = pd.read_sql_query(sql, connection)
    df = pd.DataFrame(query)

    # Close connection and return dataframe
    connection.close()
    return df

def get_qty_by_day(server, schema):
    connection = conn.get_connection(server)

    # Use query_builder to get sql
    sql = qb.qty_by_day(schema)

    # Execute sql and put in dataframe
    query = pd.read_sql_query(sql, connection)
    df = pd.DataFrame(query)

    # Close connection and return dataframe
    connection.close()
    return df

def get_ces_by_day(server, schema):
    connection = conn.get_connection(server)

    # Use query_builder to get sql
    sql = qb.ces_by_day(schema)

    # Execute sql and put in dataframe
    query = pd.read_sql_query(sql, connection)
    df = pd.DataFrame(query)

    # Close connection and return dataframe
    connection.close()
    return df

def get_depletion_by_day(server, schema):
    connection = conn.get_connection(server)

    # Use query_builder to get sql
    sql = qb.depletion_by_day(schema)

    # Execute sql and put in dataframe
    query = pd.read_sql_query(sql, connection)
    df = pd.DataFrame(query)

    # Close connection and return dataframe
    connection.close()
    return df

def get_profit_by_brand_by_day(server, schema, brand):
    connection = conn.get_connection(server)

    # Use query_builder to get sql
    sql = qb.profit_by_brand_by_day(schema, brand)

    # Execute sql and put in dataframe
    query = pd.read_sql_query(sql, connection)
    df = pd.DataFrame(query)

    # Close connection and return dataframe
    connection.close()
    return df

def get_qty_by_brand_by_day(server, schema, brand):
    connection = conn.get_connection(server)

    # Use query_builder to get sql
    sql = qb.qty_by_brand_by_day(schema, brand)

    # Execute sql and put in dataframe
    query = pd.read_sql_query(sql, connection)
    df = pd.DataFrame(query)

    # Close connection and return dataframe
    connection.close()
    return df

def get_ces_by_brand_by_day(server, schema, brand):
    connection = conn.get_connection(server)

    # Use query_builder to get sql
    sql = qb.ces_by_brand_by_day(schema, brand)

    # Execute sql and put in dataframe
    query = pd.read_sql_query(sql, connection)
    df = pd.DataFrame(query)

    # Close connection and return dataframe
    connection.close()
    return df

def get_depletion_by_brand_by_day(server, schema, brand):
    connection = conn.get_connection(server)

    # Use query_builder to get sql
    sql = qb.depletion_by_brand_by_day(schema, brand)

    # Execute sql and put in dataframe
    query = pd.read_sql_query(sql, connection)
    df = pd.DataFrame(query)

    # Close connection and return dataframe
    connection.close()
    return df

def get_profit_by_onoff_by_day(server, schema, onoff):
    connection = conn.get_connection(server)

    # Use query_builder to get sql
    sql = qb.profit_by_onoff_by_day(schema, onoff)

    # Execute sql and put in dataframe
    query = pd.read_sql_query(sql, connection)
    df = pd.DataFrame(query)

    # Close connection and return dataframe
    connection.close()
    return df

def get_qty_by_onoff_by_day(server, schema, onoff):
    connection = conn.get_connection(server)

    # Use query_builder to get sql
    sql = qb.qty_by_onoff_by_day(schema, onoff)

    # Execute sql and put in dataframe
    query = pd.read_sql_query(sql, connection)
    df = pd.DataFrame(query)

    # Close connection and return dataframe
    connection.close()
    return df

def get_ces_by_onoff_by_day(server, schema, onoff):
    connection = conn.get_connection(server)

    # Use query_builder to get sql
    sql = qb.ces_by_onoff_by_day(schema, onoff)

    # Execute sql and put in dataframe
    query = pd.read_sql_query(sql, connection)
    df = pd.DataFrame(query)

    # Close connection and return dataframe
    connection.close()
    return df

def get_depletion_by_onoff_by_day(server, schema, onoff):
    connection = conn.get_connection(server)

    # Use query_builder to get sql
    sql = qb.depletion_by_onoff_by_day(schema, onoff)

    # Execute sql and put in dataframe
    query = pd.read_sql_query(sql, connection)
    df = pd.DataFrame(query)

    # Close connection and return dataframe
    connection.close()
    return df

def get_profit_by_supplier_by_day(server, schema, supplier):
    connection = conn.get_connection(server)

    # Use query_builder to get sql
    sql = qb.profit_by_supplier_by_day(schema, supplier)

    # Execute sql and put in dataframe
    query = pd.read_sql_query(sql, connection)
    df = pd.DataFrame(query)

    # Close connection and return dataframe
    connection.close()
    return df

def get_qty_by_supplier_by_day(server, schema, supplier):
    connection = conn.get_connection(server)

    # Use query_builder to get sql
    sql = qb.qty_by_supplier_by_day(schema, supplier)

    # Execute sql and put in dataframe
    query = pd.read_sql_query(sql, connection)
    df = pd.DataFrame(query)

    # Close connection and return dataframe
    connection.close()
    return df

def get_ces_by_supplier_by_day(server, schema, supplier):
    connection = conn.get_connection(server)

    # Use query_builder to get sql
    sql = qb.ces_by_supplier_by_day(schema, supplier)

    # Execute sql and put in dataframe
    query = pd.read_sql_query(sql, connection)
    df = pd.DataFrame(query)

    # Close connection and return dataframe
    connection.close()
    return df

def get_depletion_by_supplier_by_day(server, schema, supplier):
    connection = conn.get_connection(server)

    # Use query_builder to get sql
    sql = qb.depletion_by_supplier_by_day(schema, supplier)

    # Execute sql and put in dataframe
    query = pd.read_sql_query(sql, connection)
    df = pd.DataFrame(query)

    # Close connection and return dataframe
    connection.close()
    return df

def get_profit_by_market_by_day(server, schema, market):
    connection = conn.get_connection(server)

    # Use query_builder to get sql
    sql = qb.profit_by_market_by_day(schema, market)

    # Execute sql and put in dataframe
    query = pd.read_sql_query(sql, connection)
    df = pd.DataFrame(query)

    # Close connection and return dataframe
    connection.close()
    return df

def get_qty_by_market_by_day(server, schema, market):
    connection = conn.get_connection(server)

    # Use query_builder to get sql
    sql = qb.qty_by_market_by_day(schema, market)

    # Execute sql and put in dataframe
    query = pd.read_sql_query(sql, connection)
    df = pd.DataFrame(query)

    # Close connection and return dataframe
    connection.close()
    return df

def get_ces_by_market_by_day(server, schema, market):
    connection = conn.get_connection(server)

    # Use query_builder to get sql
    sql = qb.ces_by_market_by_day(schema, market)

    # Execute sql and put in dataframe
    query = pd.read_sql_query(sql, connection)
    df = pd.DataFrame(query)

    # Close connection and return dataframe
    connection.close()
    return df

def get_depletion_by_market_by_day(server, schema, market):
    connection = conn.get_connection(server)

    # Use query_builder to get sql
    sql = qb.depletion_by_market_by_day(schema, market)

    # Execute sql and put in dataframe
    query = pd.read_sql_query(sql, connection)
    df = pd.DataFrame(query)

    # Close connection and return dataframe
    connection.close()
    return df