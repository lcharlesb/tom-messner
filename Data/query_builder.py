def zip_sql(schema):
    sql = '''
                SELECT sdimzip9.zip9dsc  AS DIM_1, 
                   tmp.zip9          AS DIM_1_ID, 
                   value11           AS ONE_MONTH_2020, 
                   value12           AS ONE_MONTH_2019, 
                   value11 - value12 AS ONE_MONTH_DIFF, 
                   ( CASE 
                       WHEN value12 = 0 THEN ( CASE 
                                                 WHEN value11 = 0 THEN 0 
                                                 WHEN value11 > 0 THEN 1 
                                                 ELSE -1 
                                               end ) 
                       ELSE ( ( value11 - value12 ) * 1.00000 / ( CASE 
                              WHEN value11 > 0 
                                   AND value12 < 0 THEN -value12 
                              ELSE value12 
                                                                  end ) ) 
                     end )           AS ONE_MONTH_PCT, 
                   value21           AS ONE_YEAR_2020, 
                   value22           AS ONE_YEAR_2019, 
                   value21 - value22 AS ONE_YEAR_DIFF, 
                   ( CASE 
                       WHEN value22 = 0 THEN ( CASE 
                                                 WHEN value21 = 0 THEN 0 
                                                 WHEN value21 > 0 THEN 1 
                                                 ELSE -1 
                                               end ) 
                       ELSE ( ( value21 - value22 ) * 1.00000 / ( CASE 
                              WHEN value21 > 0 
                                   AND value22 < 0 THEN -value22 
                              ELSE value22 
                                                                  end ) ) 
                     end )           AS ONE_YEAR_PCT 
            FROM   (SELECT A1.zip9, 
                           Sum(CASE 
                                 WHEN f1.date BETWEEN 20201201 AND 20201218 THEN f1.profit 
                                 ELSE 0 
                               end) AS VALUE11, 
                           Sum(CASE 
                                 WHEN f1.date BETWEEN 20191201 AND 20191219 THEN f1.profit 
                                 ELSE 0 
                               end) AS VALUE12, 
                           Sum(CASE 
                                 WHEN f1.date BETWEEN 20200101 AND 20201218 THEN f1.profit 
                                 ELSE 0 
                               end) AS VALUE21, 
                           Sum(CASE 
                                 WHEN f1.date BETWEEN 20190101 AND 20191219 THEN f1.profit 
                                 ELSE 0 
                               end) AS VALUE22 
                    FROM   ''' + schema + '''.idigetl.mqt57 f1, 
                           ''' + schema + '''.idigetl.dimaccts A1 
                    WHERE  A1.dist_acct = f1.dist_acct 
                           AND ( ( f1.date BETWEEN 20201201 AND 20201218 ) 
                                  OR ( f1.date BETWEEN 20191201 AND 20191219 ) 
                                  OR ( f1.date BETWEEN 20200101 AND 20201218 ) 
                                  OR ( f1.date BETWEEN 20190101 AND 20191219 ) ) 
                    GROUP  BY A1.zip9) tmp, 
                   ''' + schema + '''.idigetl.sdimzip9 SDIMZIP9 
            WHERE  tmp.zip9 = sdimzip9.zip9 
            ORDER  BY ONE_YEAR_2020 DESC;
        '''

    return sql

def profit_by_market_type_by_month_sql(schema, markets):
    sql = 'select t1.TYEAR, t1.TMON, '
    for i in range(len(markets)):
        sql += 'IsNull (SUM(CASE WHEN COTID = ' + markets[i] + ' THEN PROFIT ELSE 0 END), 0) AS "' + markets[i] + '"'
        if i < len(markets) - 1:
            sql +=', '
        else:
            sql += ' '

    sql += 'FROM ' + schema + '.idiget1.MQT30 f1, ' + schema + '.idiget1.dimtime as t1 WHERE f1.date = t1.date GROUP BY TYEAR, TMON ORDER BY TYEAR asc, TMON asc'

    return sql

def cumulative_profit_by_market_type_by_month_sql(schema, years):

    sql = 'WITH sales AS ( SELECT   f1.cotid, tmon, tmon_nam, '
    for i in range(len(years)):
        sql += 'Sum( CASE WHEN f1.date BETWEEN ' + years[i] + '0101 AND ' + years[
            i] + '1231 THEN f1.profit ELSE 0 END) AS value1' + str(i + 1)
        if i < len(years) - 1:
            sql += ', '

    sql += ' FROM ' + schema + '.idigetl.mqt30 f1, ' + schema + '.idigetl.dimtime t1 WHERE t1.date=f1.date AND (('
    for i in range(len(years)):
        sql += 'f1.date BETWEEN ' + years[i] + '0101 AND ' + years[i] + '1231)'
        if i < len(years) - 1:
            sql += ' OR ('
        else:
            sql += ') '
    sql += ' GROUP BY f1.cotid, tmon, tmon_nam ), cumulative AS ( SELECT   a.cotid, a.tmon, a.tmon_nam, '
    for i in range(len(years)):
        sql += 'Sum(b.value1' + str(i + 1) + ') AS cumulative_profit_' + years[i]
        if i < len(years) - 1:
            sql += ', '

    sql += ' FROM sales a, sales b WHERE a.cotid = b.cotid AND b.tmon <= a.tmon GROUP BY a.cotid, a.tmon, a.tmon_nam, '
    for i in range(len(years)):
        sql += 'a.value1' + str(i + 1)
        if i < len(years) - 1:
            sql += ', '
        else:
            sql += ') '

    sql += 'SELECT sdimcot.cotdsc AS market_type, tmp.cotid AS market_type_id, tmp.tmon_nam AS mnth, tmp.tmon AS mnth_id, '
    for i in range(len(years)):
        sql += 'cumulative_profit_' + years[i]
        if i < len(years) - 1:
            sql += ', '

    sql += ' FROM cumulative tmp, ' + schema + '.idigetl.sdimcot SDIMCOT WHERE tmp.cotid = sdimcot.cotid ORDER BY market_type_id, mnth_id;'

    return sql

def profit_by_month(schema):
    sql = 'select t1.TYEAR, t1.TMON, IsNull(SUM(f1.PROFIT), 0) as PROFIT FROM ' + schema + '.IDIGETL.MQT30 as f1, ' + schema + '.IDIGETL.DIMTIME as t1 WHERE f1.DATE = t1.DATE GROUP BY TYEAR, TMON ORDER BY TYEAR asc, TMON asc;'

    return sql

def profit_ces_by_month(schema):
    sql = 'select t1.TYEAR, t1.TMON, IsNull(SUM(f1.PROFIT), 0) as PROFIT, IsNull(Sum(f1.CES), 0) as CES FROM ' + schema + '.IDIGETL.MQT30 as f1, ' + schema + '.IDIGETL.DIMTIME as t1 WHERE f1.DATE = t1.DATE GROUP BY TYEAR, TMON ORDER BY TYEAR asc, TMON asc;'

    return sql

def profit_ces_by_day(schema):
    sql = 'select t1.TYEAR, t1.TMON, t1.TDAY, IsNull(SUM(f1.PROFIT), 0) as PROFIT, IsNull(Sum(f1.CES), 0) as CES FROM ' + schema + '.IDIGETL.MQT30 as f1, ' + schema + '.IDIGETL.DIMTIME as t1 WHERE f1.DATE = t1.DATE GROUP BY TYEAR, TMON, TDAY ORDER BY TYEAR asc, TMON asc, TDAY asc;'

    return sql

def profit_ces_qty_by_day(schema):
    sql = 'select t1.TYEAR, t1.TMON, t1.TDAY, IsNull(SUM(f1.PROFIT), 0) as PROFIT, IsNull(Sum(f1.CES), 0) as CES, IsNull(Sum(f1.QTY), 0) as QTY FROM ' + schema + '.IDIGETL.MQT30 as f1, ' + schema + '.IDIGETL.DIMTIME as t1 WHERE f1.DATE = t1.DATE GROUP BY TYEAR, TMON, TDAY ORDER BY TYEAR asc, TMON asc, TDAY asc;'

    return sql

def profit_ces_qty_depletion_by_day(schema):
    sql = 'select t1.TYEAR, t1.TMON, t1.TDAY, IsNull(SUM(f1.PROFIT), 0) as PROFIT, IsNull(Sum(f1.CES), 0) as CES, IsNull(Sum(f1.QTY), 0) as QTY, IsNull(Sum(f1.DEPLETION), 0) as DEPLETION FROM ' + schema + '.IDIGETL.MQT30 as f1, ' + schema + '.IDIGETL.DIMTIME as t1 WHERE f1.DATE = t1.DATE GROUP BY TYEAR, TMON, TDAY ORDER BY TYEAR asc, TMON asc, TDAY asc;'

    return sql

def profit_by_day(schema):
    sql = 'select t1.TYEAR, t1.TMON, t1.TDAY, IsNull(SUM(f1.PROFIT), 0) as PROFIT FROM ' + schema + '.IDIGETL.MQT30 as f1, ' + schema + '.IDIGETL.DIMTIME as t1 WHERE f1.DATE = t1.DATE GROUP BY TYEAR, TMON, TDAY ORDER BY TYEAR asc, TMON asc, TDAY asc;'

    return sql

def qty_by_day(schema):
    sql = 'select t1.TYEAR, t1.TMON, t1.TDAY, IsNull(Sum(f1.QTY), 0) as QTY FROM ' + schema + '.IDIGETL.MQT30 as f1, ' + schema + '.IDIGETL.DIMTIME as t1 WHERE f1.DATE = t1.DATE GROUP BY TYEAR, TMON, TDAY ORDER BY TYEAR asc, TMON asc, TDAY asc;'

    return sql

def ces_by_day(schema):
    sql = 'select t1.TYEAR, t1.TMON, t1.TDAY, IsNull(Sum(f1.CES), 0) as CES FROM ' + schema + '.IDIGETL.MQT30 as f1, ' + schema + '.IDIGETL.DIMTIME as t1 WHERE f1.DATE = t1.DATE GROUP BY TYEAR, TMON, TDAY ORDER BY TYEAR asc, TMON asc, TDAY asc;'

    return sql

def depletion_by_day(schema):
    sql = 'select t1.TYEAR, t1.TMON, t1.TDAY, IsNull(Sum(f1.DEPLETION), 0) as DEPLETION FROM ' + schema + '.IDIGETL.MQT30 as f1, ' + schema + '.IDIGETL.DIMTIME as t1 WHERE f1.DATE = t1.DATE GROUP BY TYEAR, TMON, TDAY ORDER BY TYEAR asc, TMON asc, TDAY asc;'

    return sql

def profit_by_brand_by_day(schema, brdid):
    sql = 'select t1.TYEAR, t1.TMON, t1.TDAY, IsNull(Sum(CASE WHEN f1.BRDID = \'' + brdid + '\' THEN f1.PROFIT ELSE 0 END), 0) as PROFIT FROM ' + schema + '.IDIGETL.MQT58 as f1, ' + schema + '.IDIGETL.DIMTIME as t1 WHERE f1.DATE = t1.DATE GROUP BY TYEAR, TMON, TDAY ORDER BY TYEAR asc, TMON asc, TDAY asc;'

    return sql

def qty_by_brand_by_day(schema, brdid):
    sql = 'select t1.TYEAR, t1.TMON, t1.TDAY, IsNull(Sum(CASE WHEN f1.BRDID = \'' + brdid + '\' THEN f1.QTY ELSE 0 END), 0) as QTY FROM ' + schema + '.IDIGETL.MQT58 as f1, ' + schema + '.IDIGETL.DIMTIME as t1 WHERE f1.DATE = t1.DATE GROUP BY TYEAR, TMON, TDAY ORDER BY TYEAR asc, TMON asc, TDAY asc;'

    return sql

def ces_by_brand_by_day(schema, brdid):
    sql = 'select t1.TYEAR, t1.TMON, t1.TDAY, IsNull(Sum(CASE WHEN f1.BRDID = \'' + brdid + '\' THEN f1.CES ELSE 0 END), 0) as CES FROM ' + schema + '.IDIGETL.MQT58 as f1, ' + schema + '.IDIGETL.DIMTIME as t1 WHERE f1.DATE = t1.DATE GROUP BY TYEAR, TMON, TDAY ORDER BY TYEAR asc, TMON asc, TDAY asc;'

    return sql

def depletion_by_brand_by_day(schema, brdid):
    sql = 'select t1.TYEAR, t1.TMON, t1.TDAY, IsNull(Sum(CASE WHEN f1.BRDID = \'' + brdid + '\' THEN f1.DEPLETION ELSE 0 END), 0) as DEPLETION FROM ' + schema + '.IDIGETL.MQT58 as f1, ' + schema + '.IDIGETL.DIMTIME as t1 WHERE f1.DATE = t1.DATE GROUP BY TYEAR, TMON, TDAY ORDER BY TYEAR asc, TMON asc, TDAY asc;'

    return sql

def profit_by_onoff_by_day(schema, onoff):
    sql = 'select t1.TYEAR, t1.TMON, t1.TDAY, IsNull(Sum(CASE WHEN f1.ONOFFID = \'' + onoff + '\' THEN f1.PROFIT ELSE 0 END), 0) as PROFIT FROM ' + schema + '.IDIGETL.MQT58 as f1, ' + schema + '.IDIGETL.DIMTIME as t1 WHERE f1.DATE = t1.DATE GROUP BY TYEAR, TMON, TDAY ORDER BY TYEAR asc, TMON asc, TDAY asc;'

    return sql

def qty_by_onoff_by_day(schema, onoff):
    sql = 'select t1.TYEAR, t1.TMON, t1.TDAY, IsNull(Sum(CASE WHEN f1.ONOFFID = \'' + onoff + '\' THEN f1.QTY ELSE 0 END), 0) as QTY FROM ' + schema + '.IDIGETL.MQT58 as f1, ' + schema + '.IDIGETL.DIMTIME as t1 WHERE f1.DATE = t1.DATE GROUP BY TYEAR, TMON, TDAY ORDER BY TYEAR asc, TMON asc, TDAY asc;'

    return sql

def ces_by_onoff_by_day(schema, onoff):
    sql = 'select t1.TYEAR, t1.TMON, t1.TDAY, IsNull(Sum(CASE WHEN f1.ONOFFID = \'' + onoff + '\' THEN f1.CES ELSE 0 END), 0) as CES FROM ' + schema + '.IDIGETL.MQT58 as f1, ' + schema + '.IDIGETL.DIMTIME as t1 WHERE f1.DATE = t1.DATE GROUP BY TYEAR, TMON, TDAY ORDER BY TYEAR asc, TMON asc, TDAY asc;'

    return sql

def depletion_by_onoff_by_day(schema, onoff):
    sql = 'select t1.TYEAR, t1.TMON, t1.TDAY, IsNull(Sum(CASE WHEN f1.ONOFFID = \'' + onoff + '\' THEN f1.DEPLETION ELSE 0 END), 0) as DEPLETION FROM ' + schema + '.IDIGETL.MQT58 as f1, ' + schema + '.IDIGETL.DIMTIME as t1 WHERE f1.DATE = t1.DATE GROUP BY TYEAR, TMON, TDAY ORDER BY TYEAR asc, TMON asc, TDAY asc;'

    return sql

def profit_by_supplier_by_day(schema, supplier):
    sql = 'select t1.TYEAR, t1.TMON, t1.TDAY, IsNull(Sum(CASE WHEN f1.CAT3ID = \'' + supplier + '\' THEN f1.PROFIT ELSE 0 END), 0) as PROFIT FROM ' + schema + '.IDIGETL.MQT58 as f1, ' + schema + '.IDIGETL.DIMTIME as t1 WHERE f1.DATE = t1.DATE GROUP BY TYEAR, TMON, TDAY ORDER BY TYEAR asc, TMON asc, TDAY asc;'

    return sql

def qty_by_supplier_by_day(schema, supplier):
    sql = 'select t1.TYEAR, t1.TMON, t1.TDAY, IsNull(Sum(CASE WHEN f1.CAT3ID = \'' + supplier + '\' THEN f1.QTY ELSE 0 END), 0) as QTY FROM ' + schema + '.IDIGETL.MQT58 as f1, ' + schema + '.IDIGETL.DIMTIME as t1 WHERE f1.DATE = t1.DATE GROUP BY TYEAR, TMON, TDAY ORDER BY TYEAR asc, TMON asc, TDAY asc;'

    return sql

def ces_by_supplier_by_day(schema, supplier):
    sql = 'select t1.TYEAR, t1.TMON, t1.TDAY, IsNull(Sum(CASE WHEN f1.CAT3ID = \'' + supplier + '\' THEN f1.CES ELSE 0 END), 0) as CES FROM ' + schema + '.IDIGETL.MQT58 as f1, ' + schema + '.IDIGETL.DIMTIME as t1 WHERE f1.DATE = t1.DATE GROUP BY TYEAR, TMON, TDAY ORDER BY TYEAR asc, TMON asc, TDAY asc;'

    return sql

def depletion_by_supplier_by_day(schema, supplier):
    sql = 'select t1.TYEAR, t1.TMON, t1.TDAY, IsNull(Sum(CASE WHEN f1.CAT3ID = \'' + supplier + '\' THEN f1.DEPLETION ELSE 0 END), 0) as DEPLETION FROM ' + schema + '.IDIGETL.MQT58 as f1, ' + schema + '.IDIGETL.DIMTIME as t1 WHERE f1.DATE = t1.DATE GROUP BY TYEAR, TMON, TDAY ORDER BY TYEAR asc, TMON asc, TDAY asc;'

    return sql

def profit_by_market_by_day(schema, market):
    sql = 'select t1.TYEAR, t1.TMON, t1.TDAY, IsNull(Sum(CASE WHEN f1.COTID = \'' + market + '\' THEN f1.PROFIT ELSE 0 END), 0) as PROFIT FROM ' + schema + '.IDIGETL.MQT58 as f1, ' + schema + '.IDIGETL.DIMTIME as t1 WHERE f1.DATE = t1.DATE GROUP BY TYEAR, TMON, TDAY ORDER BY TYEAR asc, TMON asc, TDAY asc;'

    return sql

def qty_by_market_by_day(schema, market):
    sql = 'select t1.TYEAR, t1.TMON, t1.TDAY, IsNull(Sum(CASE WHEN f1.COTID = \'' + market + '\' THEN f1.QTY ELSE 0 END), 0) as QTY FROM ' + schema + '.IDIGETL.MQT58 as f1, ' + schema + '.IDIGETL.DIMTIME as t1 WHERE f1.DATE = t1.DATE GROUP BY TYEAR, TMON, TDAY ORDER BY TYEAR asc, TMON asc, TDAY asc;'

    return sql

def ces_by_market_by_day(schema, market):
    sql = 'select t1.TYEAR, t1.TMON, t1.TDAY, IsNull(Sum(CASE WHEN f1.COTID = \'' + market + '\' THEN f1.CES ELSE 0 END), 0) as CES FROM ' + schema + '.IDIGETL.MQT58 as f1, ' + schema + '.IDIGETL.DIMTIME as t1 WHERE f1.DATE = t1.DATE GROUP BY TYEAR, TMON, TDAY ORDER BY TYEAR asc, TMON asc, TDAY asc;'

    return sql

def depletion_by_market_by_day(schema, market):
    sql = 'select t1.TYEAR, t1.TMON, t1.TDAY, IsNull(Sum(CASE WHEN f1.COTID = \'' + market + '\' THEN f1.DEPLETION ELSE 0 END), 0) as DEPLETION FROM ' + schema + '.IDIGETL.MQT58 as f1, ' + schema + '.IDIGETL.DIMTIME as t1 WHERE f1.DATE = t1.DATE GROUP BY TYEAR, TMON, TDAY ORDER BY TYEAR asc, TMON asc, TDAY asc;'

    return sql

def profit_ces_qty_by_week(schema):
    sql = 'select t1.TYEAR, t1.TWKYR, IsNull(SUM(f1.PROFIT), 0) as PROFIT, IsNull(Sum(f1.CES), 0) as CES, IsNull(Sum(f1.QTY), 0) as QTY FROM ' + schema + '.IDIGETL.MQT30 as f1, ' + schema + '.IDIGETL.DIMTIME as t1 WHERE f1.DATE = t1.DATE GROUP BY TYEAR, TWKYR ORDER BY TYEAR asc, TWKYR asc;'

    return sql

def brand_family_by_day(schema, brand_family, brands):
    sql = 'select t1.TYEAR, t1.TMON, t1.TDAY, '
    for i in range(len(brands)):
        sql += 'IsNull (SUM(CASE WHEN I1.BRDID = \'' + brands[i] + '\' THEN f1.QTY ELSE 0 END), 0) AS "' + brands[i] + '"'
        if i < len(brands) - 1:
            sql += ', '
        else:
            sql += ' '

    sql += 'FROM ' + schema + '.IDIGETL.MQT42 as f1, ' + schema + '.IDIGETL.DIMTIME as t1, ' + schema + '.IDIGETL.DIMITEM as I1 WHERE f1.DATE = t1.DATE and I1.ITEM = f1.ITEM and I1.CAT2ID = \'' + brand_family + '\' GROUP BY t1.TYEAR, t1.TMON, t1.TDAY ORDER BY t1.TYEAR, t1.TMON, t1.TDAY'

    return sql

