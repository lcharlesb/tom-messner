How to get predictions:
    - Run this script with the following arguments:
        --schema [SCHEMA]                           <-- schema for company, ex: WCS01600
        --server [SERVER]                           <-- server that schema lives on, ex: VT3
        --numdays [INT]                             <-- number of days into the future for prediction, ex: 31
        --feature [PROFIT|CES|QTY|DEPLETION]        <-- feature to be predicted, ex: DEPLETION
        --drill [BRAND|ONOFF|SUPPLIER|MARKET]       <-- OPTIONAL, dimension to drill to, ex: BRAND
        --drillvalue [VALUE]                        <-- OPTIONAL, value to drill into, ex: B270
    - Predictions will be in StdOut
    - Predictions for the next [numdays] days

    Example C# code to run the script:

        private void run_script(string schema, string server, int days, string feature)
        {
            ProcessStartInfo start = new ProcessStartInfo();
            start.FileName = "path/to/script.py";
            start.Arguments = string.Format("{0} {1} {2} {3} {4} {5} {6} {7}", "--schema", schema, "--server", server, "--numdays", days, "--feature", feature);
            start.UseShellExecute = false;
            start.RedirectStandardOutput = true;
            using(Process process = Process.Start(start))
            {
                using(StreamReader reader = process.StandardOutput)
                {
                    string result = reader.ReadToEnd();
                    return result.Split(' ')
                }
            }
        }

Description of script:
    This script uses recurrent neural networks (LSTM models) to predict future values for any company within our system.
    Given inputted arguments, the script queries our DB and pulls the necessary info in the form of a DataFrame.
    The DataFrame is then preprocessed - date index, add missing dates, split, and normalized.
    The model is trained on the historical data of the given schema.
    The model predicts future values of the given feature for the given number of days.
        IMPORTANT: Keep in mind that the more days predicted into the future, the less accurate predictions will be.
    The predictions are outputted into stdout, which can be read by C#, in the form of an array of float32.

How to speed up script:
    If speed becomes an issue, the script can pull predictions much faster if it does not train the model each time it
    is called. To do this, the function compile_and_fit() within the WindowGenerator class has a call to model.fit().
    Instead of calling this each time, cache the model object after it has been trained (model.fit()) for each company.
    When the script is called, check if the schema exists in the cache. If it does, pull the model and ignore
    compile_and_fit() completely, simply call predict() on the model. This will make it so trained models are cached,
    saving significant time.

    This being said, the models should be trained occasionally. New data will make the models more accurate. A time
    limit within the cache could address this issue.

    The hardest part of this process will be linking our current cache into this python script. If it's not easy to do,
    make a function that returns the model to C# and cache it there, and add an extra argument into this script that
    says whether to train the model or use a cached model that is passed in.